package cl.thisisalexis.globallogicchallenge.model.earthquake;

import cl.thisisalexis.common.core.workflow.ExecutorRequest;
import javafx.util.Builder;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A POJO used to define filters to sent to Earthquake services
 *
 * @author Alexis Bravo
 */
public class EarthquakeQueryFilter implements ExecutorRequest {

    private LocalDate starTime;
    private LocalDate endTime;
    private BigDecimal maxMagnitude;
    private BigDecimal minMagnitude;

    private BigDecimal latitude;
    private BigDecimal longitude;
    private BigDecimal maxRadius;
    private BigDecimal maxRadiusKm;

    public LocalDate getStarTime() {
        return starTime;
    }

    public void setStarTime(LocalDate starTime) {
        this.starTime = starTime;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getMaxMagnitude() {
        return maxMagnitude;
    }

    public void setMaxMagnitude(BigDecimal maxMagnitude) {
        this.maxMagnitude = maxMagnitude;
    }

    public BigDecimal getMinMagnitude() {
        return minMagnitude;
    }

    public void setMinMagnitude(BigDecimal minMagnitude) {
        this.minMagnitude = minMagnitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(BigDecimal maxRadius) {
        this.maxRadius = maxRadius;
    }

    public BigDecimal getMaxRadiusKm() {
        return maxRadiusKm;
    }

    public void setMaxRadiusKm(BigDecimal maxRadiusKm) {
        this.maxRadiusKm = maxRadiusKm;
    }

    public static class EarthquakeQueryFilterBuilder implements Builder<EarthquakeQueryFilter> {

        private LocalDate starTime;
        private LocalDate endTime;
        private BigDecimal maxMagnitude;
        private BigDecimal minMagnitude;
        private BigDecimal latitude;
        private BigDecimal longitude;
        private BigDecimal maxRadius;
        private BigDecimal maxRadiusKm;

        public static EarthquakeQueryFilterBuilder getInstance() {
            return new EarthquakeQueryFilterBuilder();
        }

        public EarthquakeQueryFilterBuilder withStarTime(LocalDate starTime) {
            this.starTime = starTime;
            return this;
        }

        public EarthquakeQueryFilterBuilder withEndTime(LocalDate endTime) {
            this.endTime = endTime;
            return this;
        }

        public EarthquakeQueryFilterBuilder withMaxMagnitude(BigDecimal maxMagnitude) {
            this.maxMagnitude = maxMagnitude;
            return this;
        }

        public EarthquakeQueryFilterBuilder withMinMagnitude(BigDecimal minMagnitude) {
            this.minMagnitude = minMagnitude;
            return this;
        }

        public EarthquakeQueryFilterBuilder withLatitude(BigDecimal latitude) {
            this.latitude = latitude;
            return this;
        }

        public EarthquakeQueryFilterBuilder withLongitude(BigDecimal longitude) {
            this.longitude = longitude;
            return this;
        }

        public EarthquakeQueryFilterBuilder withMaxRadius(BigDecimal maxRadius) {
            this.maxRadius = maxRadius;
            return this;
        }

        public EarthquakeQueryFilterBuilder withMaxRadiusKm(BigDecimal maxRadiusKm) {
            this.maxRadiusKm = maxRadiusKm;
            return this;
        }

        @Override
        public EarthquakeQueryFilter build() {
            EarthquakeQueryFilter filter = new EarthquakeQueryFilter();
            filter.setStarTime(this.starTime);
            filter.setEndTime(this.endTime);
            filter.setMaxMagnitude(this.maxMagnitude);
            filter.setMinMagnitude(this.minMagnitude);
            filter.setLatitude(this.latitude);
            filter.setLongitude(this.longitude);
            filter.setMaxRadius(this.maxRadius);
            filter.setMaxRadiusKm(this.maxRadiusKm);

            return filter;
        }

    }


}
