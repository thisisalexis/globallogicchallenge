package cl.thisisalexis.globallogicchallenge.model.earthquake;

import cl.thisisalexis.common.core.workflow.ExecutorRequest;
import javafx.util.Builder;

import java.time.LocalDate;

/**
 * A POJO used to define filters to sent to Earthquake services
 *
 * @author Alexis Bravo
 */
public class EarthquakeQueryFilterByPeriod implements ExecutorRequest {

    private LocalDate firstPeriodStartDate;
    private LocalDate firstPeriodEndDate;
    private LocalDate lastPeriodStartDate;
    private LocalDate lastPeriodEndDate;

    public LocalDate getFirstPeriodStartDate() {
        return firstPeriodStartDate;
    }

    public void setFirstPeriodStartDate(LocalDate firstPeriodStartDate) {
        this.firstPeriodStartDate = firstPeriodStartDate;
    }

    public LocalDate getFirstPeriodEndDate() {
        return firstPeriodEndDate;
    }

    public void setFirstPeriodEndDate(LocalDate firstPeriodEndDate) {
        this.firstPeriodEndDate = firstPeriodEndDate;
    }

    public LocalDate getLastPeriodStartDate() {
        return lastPeriodStartDate;
    }

    public void setLastPeriodStartDate(LocalDate lastPeriodStartDate) {
        this.lastPeriodStartDate = lastPeriodStartDate;
    }

    public LocalDate getLastPeriodEndDate() {
        return lastPeriodEndDate;
    }

    public void setLastPeriodEndDate(LocalDate lastPeriodEndDate) {
        this.lastPeriodEndDate = lastPeriodEndDate;
    }

    public static class EarthquakeQueryFilterByPeriodBuilder implements Builder<EarthquakeQueryFilterByPeriod> {

        private LocalDate firstPeriodStartDate;
        private LocalDate firstPeriodEndDate;
        private LocalDate lastPeriodStartDate;
        private LocalDate lastPeriodEndDate;

        public EarthquakeQueryFilterByPeriodBuilder withFirstPeriodStartDate(LocalDate firstPeriodStartDate) {
            this.firstPeriodStartDate = firstPeriodStartDate;
            return this;
        }

        public EarthquakeQueryFilterByPeriodBuilder withFirstPeriodEndDate(LocalDate firstPeriodEndDate) {
            this.firstPeriodEndDate = firstPeriodEndDate;
            return this;
        }

        public EarthquakeQueryFilterByPeriodBuilder withLastPeriodStartDate(LocalDate lastPeriodStartDate) {
            this.lastPeriodStartDate = lastPeriodStartDate;
            return this;
        }

        public EarthquakeQueryFilterByPeriodBuilder withLastPeriodEndDate(LocalDate lastPeriodEndDate) {
            this.lastPeriodEndDate = lastPeriodEndDate;
            return this;
        }

        @Override
        public EarthquakeQueryFilterByPeriod build() {
            EarthquakeQueryFilterByPeriod earthquakeQueryFilterByPeriod = new EarthquakeQueryFilterByPeriod();

            earthquakeQueryFilterByPeriod.setFirstPeriodStartDate(this.firstPeriodStartDate);
            earthquakeQueryFilterByPeriod.setFirstPeriodEndDate(this.firstPeriodEndDate);
            earthquakeQueryFilterByPeriod.setLastPeriodStartDate(this.lastPeriodStartDate);
            earthquakeQueryFilterByPeriod.setLastPeriodEndDate(this.lastPeriodEndDate);

            return earthquakeQueryFilterByPeriod;
        }
    }

}
