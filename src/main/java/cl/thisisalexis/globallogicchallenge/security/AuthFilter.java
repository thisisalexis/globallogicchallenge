package cl.thisisalexis.globallogicchallenge.security;

import cl.thisisalexis.globallogicchallenge.property.AuthProperty;
import cl.thisisalexis.globallogicchallenge.service.auth.ApplicationTokenService;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Filter used by http interceptors to take Authorization header and decode JWT to check if this is a valid token
 *
 * @author Alexis Bravo
 */
@Component
public class AuthFilter extends OncePerRequestFilter {

    @Autowired
    private AuthProperty authProperty;

    @Autowired
    private ApplicationTokenService applicationTokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        try {
            if (existsJWT(request, response)) {
                if (!isSignedJwt(request)) {
                    throw new MalformedJwtException("Not valid jwt!");
                }
                verifyJwt(request);
            }
            chain.doFilter(request, response);
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            return;
        }
    }

    private void verifyJwt(HttpServletRequest request) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
        String jwtToken = request.getHeader(authProperty.getHeaderName());
        applicationTokenService.verifyJwt(jwtToken);
    }

    private Boolean isSignedJwt(HttpServletRequest request) {
        String jwtToken = request.getHeader(authProperty.getHeaderName());
        return applicationTokenService.isSignedJwt(jwtToken);
    }

    private boolean existsJWT(HttpServletRequest request, HttpServletResponse res) {
        String authenticationHeader = request.getHeader(authProperty.getHeaderName());
        return authenticationHeader != null;
    }

    /**
     * This method is used to valid which HttpRequest should not be filtered with the current Filter instance
     * @param request An HttpRequest
     * @return true if this filter should not be applied to the current Http request
     * @throws ServletException
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        final String contextPath = request.getContextPath();
        final String allowedPatternRegex = contextPath.concat("/auth/visitor|")
                .concat(contextPath).concat("/v2/api-docs|^")
                .concat(contextPath).concat("/swagger-resources|")
                .concat(contextPath).concat("/swagger-ui.html|^")
                .concat(contextPath).concat("/webjars/");

        Pattern allowedPattern = Pattern.compile(allowedPatternRegex);
        Matcher matcher = allowedPattern.matcher(request.getRequestURI());

        return matcher.find();
    }

}
