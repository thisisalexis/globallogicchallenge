package cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model;

import java.util.List;

public class Query {

    private List<Features> features;
    private Metadata metadata;
    private List<String> bbox;
    private String type;

    public List<Features> getFeatures() {
        return features;
    }

    public void setFeatures(List<Features> features) {
        this.features = features;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<String> getBbox() {
        return bbox;
    }

    public void setBbox(List<String> bbox) {
        this.bbox = bbox;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

