package cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.service;

import cl.thisisalexis.common.core.rest.AbstractRestService;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilter;
import cl.thisisalexis.globallogicchallenge.property.USGSEventQueryWebServiceProperty;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * A concrete implementation of AbstractRestService that establish a connection with the USGS earthquake rest service
 * to get events given a list of query parameters or filters
 *
 * @author Alexis Bravo
 */
@Service
@ConfigurationProperties(value = "external-resource.api.usgs.event.query")
public class USGSEventQueryWebService extends AbstractRestService<Query, Object> {

    public static final BigDecimal DEFAULT_RADIUS_VALUE = BigDecimal.valueOf(90L);

    @Autowired
    private USGSEventQueryWebServiceProperty webServiceProperty;

    @Override
    protected Class<Query> getClassType() {
        return Query.class;
    }

    /**
     * Connect to the USGS earthquake rest service to get a list of events given a list of filters or conditions
     *
     * @param earthquakeQueryFilter
     * @return A query object that represents the web service response
     */
    public Query execute(EarthquakeQueryFilter earthquakeQueryFilter) {
        prepareCoordinates(earthquakeQueryFilter);
        Map<String, String> queryParams = getQueryParams(earthquakeQueryFilter);
        return this.get(null, getHttpHeaders(), null, queryParams);
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        return httpHeaders;
    }

    private Map<String, String> getQueryParams(EarthquakeQueryFilter earthquakeQueryFilter) {

        Map<String, String> queryParams = new HashMap<>();

        addQueryParam(webServiceProperty.getFormat(), webServiceProperty.getFormatGeoJsonValue(), queryParams);

        if (null != earthquakeQueryFilter) {
            addQueryParam(webServiceProperty.getStartTime(), earthquakeQueryFilter.getStarTime(), queryParams);
            addQueryParam(webServiceProperty.getEndTime(), earthquakeQueryFilter.getEndTime(), queryParams);
            addQueryParam(webServiceProperty.getLatitude(), earthquakeQueryFilter.getLatitude(), queryParams);
            addQueryParam(webServiceProperty.getLongitude(), earthquakeQueryFilter.getLongitude(), queryParams);
            addQueryParam(webServiceProperty.getMaxRadius(), earthquakeQueryFilter.getMaxRadius(), queryParams);
            addQueryParam(webServiceProperty.getMaxRadiusKm(), earthquakeQueryFilter.getMaxRadiusKm(), queryParams);
            addQueryParam(webServiceProperty.getMaxMagnitude(), earthquakeQueryFilter.getMaxMagnitude(), queryParams);
            addQueryParam(webServiceProperty.getMinMagnitude(), earthquakeQueryFilter.getMinMagnitude(), queryParams);
        }

        return queryParams;
    }

    private void addQueryParam(String paramName, Object paramValue, Map<String, String> queryParamsRef) {
        if (null != paramName && null != paramValue) {
            queryParamsRef.put(paramName, paramValue.toString());
        }
    }

    private void prepareCoordinates(EarthquakeQueryFilter filter) {
        if (isRequiredRadiusParam(filter) && isNotSpecifiedRadius(filter)) {
            filter.setMaxRadius(DEFAULT_RADIUS_VALUE);
        }
    }

    private Boolean isRequiredRadiusParam(EarthquakeQueryFilter filter) {
        return null != filter.getLatitude() || null != filter.getLongitude();
    }

    private Boolean isNotSpecifiedRadius(EarthquakeQueryFilter filter) {
        return null == filter.getMaxRadius() && null == filter.getMaxRadiusKm();
    }


}
