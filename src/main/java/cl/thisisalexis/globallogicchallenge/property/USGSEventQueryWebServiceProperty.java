package cl.thisisalexis.globallogicchallenge.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Access properties related to USGS earthquake service parameters names
 *
 * @author Alexis Bravo
 */
@Component
@ConfigurationProperties("external-resource.api.usgs.event.query.param-name")
public class USGSEventQueryWebServiceProperty {

    private String endTime;
    private String startTime;
    private String latitude;
    private String longitude;
    private String maxRadius;
    private String maxRadiusKm;
    private String maxMagnitude;
    private String minMagnitude;
    private String format;
    private String formatGeoJsonValue;

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(String maxRadius) {
        this.maxRadius = maxRadius;
    }

    public String getMaxRadiusKm() {
        return maxRadiusKm;
    }

    public void setMaxRadiusKm(String maxRadiusKm) {
        this.maxRadiusKm = maxRadiusKm;
    }

    public String getMaxMagnitude() {
        return maxMagnitude;
    }

    public void setMaxMagnitude(String maxMagnitude) {
        this.maxMagnitude = maxMagnitude;
    }

    public String getMinMagnitude() {
        return minMagnitude;
    }

    public void setMinMagnitude(String minMagnitude) {
        this.minMagnitude = minMagnitude;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFormatGeoJsonValue() {
        return formatGeoJsonValue;
    }

    public void setFormatGeoJsonValue(String formatGeoJsonValue) {
        this.formatGeoJsonValue = formatGeoJsonValue;
    }
}
