package cl.thisisalexis.globallogicchallenge.service.auth;

import cl.thisisalexis.common.core.exception.AbstractAppException;
import cl.thisisalexis.common.core.service.AbstractExecutorService;
import cl.thisisalexis.common.core.workflow.EmptyExecutorRequest;
import cl.thisisalexis.globallogicchallenge.api.model.AuthorizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service that handles all JWT token functionalities
 *
 * @author Alexis Bravo
 */
@Service
public class AuthService extends AbstractExecutorService<EmptyExecutorRequest, AuthorizationResponse> {

    @Autowired
    private ApplicationTokenService applicationTokenService;

    @Override
    public AuthorizationResponse execute() throws AbstractAppException {
        String jwt = applicationTokenService.getJwt();
        AuthorizationResponse authorizationResponse = new AuthorizationResponse(jwt);
        return authorizationResponse;
    }

}
