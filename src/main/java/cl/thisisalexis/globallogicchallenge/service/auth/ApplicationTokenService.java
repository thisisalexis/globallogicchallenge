package cl.thisisalexis.globallogicchallenge.service.auth;

import cl.thisisalexis.globallogicchallenge.property.AuthProperty;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * A service meant to encapsulate all JWT token operations
 *
 * @author Alexis Bravo
 */
@Service
public class ApplicationTokenService {

    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    @Autowired
    private AuthProperty authProperty;

    /**
     * Creates a JWT given an ApplicationToken instance
     * @return a JWT signed using app secret key
     */
    public String getJwt() {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(authProperty.getSecret());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, SIGNATURE_ALGORITHM.getJcaName());
        JwtBuilder builder = Jwts.builder()
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + authProperty.getExpiration()))
                .signWith(SIGNATURE_ALGORITHM, signingKey);
        return  builder.compact();
    }

    public String getValue(String jwt, String claimName) {
        return (String) Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(authProperty.getSecret()))
                .parseClaimsJws(jwt).getBody().get(claimName);
    }

    /**
     * Verify if JTW is valid
     *
     * @return true if jwt is valid token
     */
    public Boolean isSignedJwt(String jwt) {
        return isSignedJwt(jwt, authProperty.getSecret());
    }

    /**
     * Verify if JTW is valid
     * @param jwt token
     * @param secret secret used to sign token
     * @return true if is valid and signed token
     */
    public static final Boolean isSignedJwt(String jwt, String secret) {
        return Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(secret)).isSigned(jwt);
    }

    /**
     * Decode JWT and get a list of claims
     * @param jwt JSON web token encoded
     * @return Claims instance
     */
    public Claims getClaims(String jwt) {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(authProperty.getSecret()))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    /**
     * Decode JWT using the sign and if it is not valid, throws an exception
     * @param jwt JSON web token encoded
     * @throws UnsupportedJwtException  if the {@code jwt} argument does not represent an Claims JWS
     * @throws MalformedJwtException    if the {@code jwt} string is not a valid JWS
     * @throws SignatureException       if the {@code jwt} JWS signature validation fails
     * @throws ExpiredJwtException      if the specified JWT is a Claims JWT and the Claims has an expiration time
     *                                  before the time this method is invoked.
     * @throws IllegalArgumentException if the {@code claimsJws} string is {@code null} or empty or only whitespace
     */
    public void verifyJwt(String jwt) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException {
        Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(authProperty.getSecret()))
                .parseClaimsJws(jwt);
    }

}
