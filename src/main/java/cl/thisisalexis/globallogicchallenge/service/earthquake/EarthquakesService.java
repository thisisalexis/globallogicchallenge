package cl.thisisalexis.globallogicchallenge.service.earthquake;

import cl.thisisalexis.common.core.exception.AbstractAppException;
import cl.thisisalexis.common.core.service.AbstractExecutorService;
import cl.thisisalexis.globallogicchallenge.api.model.FeaturesResponse;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilter;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Query;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.service.USGSEventQueryWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A Service that acts as a Facade to access the USGS earthquake rest service, get earthquakes information from it
 * and prepare it t be sent through the application public API
 *
 * @author ALexis Bravo
 */
@Service
public class EarthquakesService extends AbstractExecutorService<EarthquakeQueryFilter, FeaturesResponse> {

    @Autowired
    USGSEventQueryWebService usgsEventQueryWebService;

    /**
     *  Execute the service Executor, get and prepare list of earthquakes from external rest service
     * @param executorParam which specifies filters or conditions to look for earthquakes
     * @return a FeaturesResponse object containing a list of features, events or earthquakes
     * @throws AbstractAppException
     */
    @Override
    public FeaturesResponse execute(EarthquakeQueryFilter executorParam) throws AbstractAppException {

        FeaturesResponse featuresResponse = getResponseFromWs(executorParam);
        return featuresResponse;
    }

    private FeaturesResponse getResponseFromWs(EarthquakeQueryFilter executorParam) {
        FeaturesResponse featuresResponse = new FeaturesResponse();
        Query restServiceResponse = usgsEventQueryWebService.execute(executorParam);
        if (null != restServiceResponse) {
            featuresResponse.setFeatures(restServiceResponse.getFeatures());
        }
        return featuresResponse;
    }

}
