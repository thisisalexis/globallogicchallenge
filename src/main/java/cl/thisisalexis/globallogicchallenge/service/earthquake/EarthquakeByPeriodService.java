package cl.thisisalexis.globallogicchallenge.service.earthquake;

import cl.thisisalexis.common.core.exception.AbstractAppException;
import cl.thisisalexis.common.core.service.AbstractExecutorService;
import cl.thisisalexis.globallogicchallenge.api.model.FeaturesResponse;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilter;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilterByPeriod;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Features;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Get a list of earthquakes between a range of magnitudes
 *
 * @author Alexis Bravo
 */
@Service
public class EarthquakeByPeriodService
        extends AbstractExecutorService<EarthquakeQueryFilterByPeriod, FeaturesResponse> {

    @Autowired
    private EarthquakesService earthquakesService;

    @Override
    public FeaturesResponse execute(EarthquakeQueryFilterByPeriod filter) throws AbstractAppException {

        FeaturesResponse featuresResponse = new FeaturesResponse();

        List<Features> firstPeriodFeatures = getFeaturesFromPeriod(filter.getFirstPeriodStartDate(), filter.getFirstPeriodEndDate());
        List<Features> lastPeriodFeatures = getFeaturesFromPeriod(filter.getLastPeriodStartDate(), filter.getLastPeriodEndDate());

        featuresResponse.getFeatures().addAll(firstPeriodFeatures);
        featuresResponse.getFeatures().addAll(lastPeriodFeatures);

        return featuresResponse;
    }

    private List<Features> getFeaturesFromPeriod(LocalDate from, LocalDate to) throws AbstractAppException {

        List<Features> features = new ArrayList<>();

        EarthquakeQueryFilter earthquakeQueryFilter = EarthquakeQueryFilter.EarthquakeQueryFilterBuilder.getInstance()
                .withStarTime(from)
                .withEndTime(to)
                .build();

        FeaturesResponse webServiceResponse = earthquakesService.execute(earthquakeQueryFilter);

        if (null != webServiceResponse) {
            features = webServiceResponse.getFeatures();
        }

        return features;

    }

}
