package cl.thisisalexis.globallogicchallenge.api.controller.earthquake;

import cl.thisisalexis.common.core.api.AbstractApiController;
import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilter;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilterByPeriod;
import cl.thisisalexis.globallogicchallenge.service.earthquake.EarthquakeByPeriodService;
import cl.thisisalexis.globallogicchallenge.service.earthquake.EarthquakesService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * A class that encapsulates or contains all earthquake search related API methods
 *
 * @autor Alexis Bravo
 */
@RestController
@RequestMapping("earthquakes")
public class EarthquakeApiController extends AbstractApiController implements EarthquakeDocumentedApi {

    @Override
    @GetMapping(value = "/by-date/from/{from}/to/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutorResponse> getEarthquakesBetweenDates(
            @RequestHeader("Authorization") String authorization,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate from,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate to) {

        EarthquakeQueryFilter earthquakeQueryFilter = new EarthquakeQueryFilter.EarthquakeQueryFilterBuilder()
                .withStarTime(from)
                .withEndTime(to)
                .build();

        this.apiWorkflowExecutor.setExecutor(EarthquakesService.class);
        this.apiWorkflowExecutor.setExecutorRequest(earthquakeQueryFilter);

        return apiWorkflowExecutor.execute();
    }

    @Override
    @GetMapping(value = "/by-magnitude/from/{minMagnitude}/to/{maxMagnitude}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutorResponse> getEarthquakesBetweenMagnitudes(@RequestHeader("Authorization") String authorization,
                                                                            @PathVariable BigDecimal minMagnitude,
                                                                            @PathVariable BigDecimal maxMagnitude) {

        EarthquakeQueryFilter earthquakeQueryFilter = new EarthquakeQueryFilter.EarthquakeQueryFilterBuilder()
                .withMinMagnitude(minMagnitude)
                .withMaxMagnitude(maxMagnitude)
                .build();

        this.apiWorkflowExecutor.setExecutor(EarthquakesService.class);
        this.apiWorkflowExecutor.setExecutorRequest(earthquakeQueryFilter);

        return apiWorkflowExecutor.execute();
    }

    @Override
    @GetMapping(value = "/by-period/{firstPeriodFrom}/{firstPeriodTo}/{lastPeriodFrom}/{lastPeriodTo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutorResponse> getEarthquakesByPeriods(@RequestHeader("Authorization") String authorization,
                                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate firstPeriodFrom,
                                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate firstPeriodTo,
                                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate lastPeriodFrom,
                                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable LocalDate lastPeriodTo) {
        EarthquakeQueryFilterByPeriod filter = new EarthquakeQueryFilterByPeriod.EarthquakeQueryFilterByPeriodBuilder()
                .withFirstPeriodStartDate(firstPeriodFrom)
                .withFirstPeriodEndDate(firstPeriodTo)
                .withLastPeriodStartDate(lastPeriodFrom)
                .withLastPeriodEndDate(lastPeriodTo)
                .build();

        this.apiWorkflowExecutor.setExecutor(EarthquakeByPeriodService.class);
        this.apiWorkflowExecutor.setExecutorRequest(filter);

        return apiWorkflowExecutor.execute();
    }

    @Override
    @GetMapping(value = "/by-country/{latitude}/{longitude}")
    public ResponseEntity<ExecutorResponse> getEarthquakesByCountryCoordinates(@RequestHeader("Authorization") String authorization,
                                                                               @PathVariable BigDecimal latitude,
                                                                               @PathVariable BigDecimal longitude) {
        EarthquakeQueryFilter earthquakeQueryFilter = new EarthquakeQueryFilter.EarthquakeQueryFilterBuilder()
                .withLatitude(latitude)
                .withLongitude(longitude)
                .build();

        this.apiWorkflowExecutor.setExecutor(EarthquakesService.class);
        this.apiWorkflowExecutor.setExecutorRequest(earthquakeQueryFilter);

        return apiWorkflowExecutor.execute();
    }

}
