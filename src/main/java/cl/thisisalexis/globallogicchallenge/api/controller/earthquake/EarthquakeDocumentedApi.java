package cl.thisisalexis.globallogicchallenge.api.controller.earthquake;

import cl.thisisalexis.common.core.api.DocumentedApi;
import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.api.model.FeaturesResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestHeader;

import java.math.BigDecimal;
import java.time.LocalDate;

@Api(value = "Earthquakes API", tags = "earthquakes")
public interface EarthquakeDocumentedApi extends DocumentedApi {

    @ApiOperation(value = "Get a list of earthquakes and its details between two dates", response = FeaturesResponse.class)
    ResponseEntity<ExecutorResponse> getEarthquakesBetweenDates(@ApiParam("JWT") @RequestHeader("Authorization") String authorization,
                                                                @ApiParam("Starting date to checkout for events") LocalDate from,
                                                                @ApiParam("Ending date to checkout for events") LocalDate to);

    @ApiOperation(value = "Get a list of earthquakes and its details given a max and a min magnitude", response = FeaturesResponse.class)
    ResponseEntity<ExecutorResponse> getEarthquakesBetweenMagnitudes(@ApiParam("JWT") @RequestHeader("Authorization") String authorization,
                                                                     @ApiParam("Minimum magnitude") BigDecimal minMagnitude,
                                                                     @ApiParam("Maximum magnitude") BigDecimal maxMagnitude);

    @ApiOperation(value = "Get a list of earthquakes and its details given two periods", response = FeaturesResponse.class)
    ResponseEntity<ExecutorResponse> getEarthquakesByPeriods(@ApiParam("JWT") @RequestHeader("Authorization") String authorization,
                                                             @ApiParam("First period starting date to checkout for events") LocalDate firstPeriodFrom,
                                                             @ApiParam("First period ending date to checkout for events") LocalDate firstPeriodTo,
                                                             @ApiParam("Last period starting date to checkout for events") LocalDate lastPeriodFrom,
                                                             @ApiParam("Last period ending date to checkout for events") LocalDate lastPeriodTo);

    @ApiOperation(value = "Get a list of earthquakes and its details given a country geographical points", response = FeaturesResponse.class)
    ResponseEntity<ExecutorResponse> getEarthquakesByCountryCoordinates(@ApiParam("JWT") @RequestHeader("Authorization") String authorization,
                                                                        @ApiParam("Latitude point") BigDecimal latitude,
                                                                        @ApiParam("MLatitude point") BigDecimal longitude);


}
