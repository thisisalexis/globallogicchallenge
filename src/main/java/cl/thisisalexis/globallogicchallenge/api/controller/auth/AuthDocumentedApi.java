package cl.thisisalexis.globallogicchallenge.api.controller.auth;

import cl.thisisalexis.common.core.api.DocumentedApi;
import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.api.model.AuthorizationResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;

@Api(value = "Authorization API", tags = {"auth"})
public interface AuthDocumentedApi extends DocumentedApi {

    @ApiOperation(value = "Given a visitor (not registered user) it retrieves a valid token so it can access public endpoints", response = AuthorizationResponse.class)
    ResponseEntity<ExecutorResponse> getVisitorAuthorization();

}
