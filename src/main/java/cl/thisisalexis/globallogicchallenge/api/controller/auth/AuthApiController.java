package cl.thisisalexis.globallogicchallenge.api.controller.auth;

import cl.thisisalexis.common.core.api.AbstractApiController;
import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.service.auth.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth")
public class AuthApiController extends AbstractApiController implements AuthDocumentedApi {

    @GetMapping("/visitor")
    @Override
    public ResponseEntity<ExecutorResponse> getVisitorAuthorization() {
        this.apiWorkflowExecutor.setExecutor(AuthService.class);
        return apiWorkflowExecutor.execute();
    }

}
