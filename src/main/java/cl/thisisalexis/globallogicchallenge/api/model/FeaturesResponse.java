package cl.thisisalexis.globallogicchallenge.api.model;

import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Features;

import java.util.ArrayList;
import java.util.List;

/***
 * A wrapper class containing a list of features
 *
 * @author Alexis Bravo
 */
public class FeaturesResponse implements ExecutorResponse {

    private List<Features> features;

    public List<Features> getFeatures() {
        if (null == this.features) {
            this.features = new ArrayList<>();
        }
        return features;
    }

    public void setFeatures(List<Features> features) {
        this.features = features;
    }

}
