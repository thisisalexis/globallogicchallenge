package cl.thisisalexis.globallogicchallenge.api.model;

import cl.thisisalexis.common.core.workflow.ExecutorResponse;

/**
 * A POJO used to define AuthorizationResponse
 *
 * @author Alexis Bravo
 */
public class AuthorizationResponse implements ExecutorResponse {

    private String token;

    public AuthorizationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
