GLOBAL LOGIC CHALLENGE DOCUMENTATION


This is an application that acts as an intermediate between clients and the USGS Earthquake event API 

Download:

Go to your git bash and clone the source code of the project by typing:

git clone https://thisisalexis@bitbucket.org/thisisalexis/globallogicchallenge.git

Generate executable:

Go to project route and type the command

- From shell (command line), go to the root of the project you just downloaded. 
- Execute the next command from shell or command line: gradlew bootJar. This action should have created the file build/libs/globallogicchallenge-0.0.1-SNAPSHOT.jar in the project 
- From command line (in the root directory of the project) execute the next command: java -jar build/libs/globallogicchallenge-0.0.1-SNAPSHOT.jar 
- After this, the application should have started at 8097 port (If required, you can change that port in the application-dev.properties file) 
- Now you are ready to use the exposed endpoints; follow the instructions in the next section to do so.

SEE THE WORKING APPLICATION AND API DOCUMENTATION

- Enter to http://localhost:8097/usgs-interface/swagger-ui.html and there you'll see a list of endpoints exposed by the application and all of its documentation(If you set another port, consider to change it here :))

USING THE APPLICATION: Get a JWT

- To log in in the application (and get a JWT), got to Enter to http://localhost:8097/usgs-interface/swagger-ui.html
- Go to the Auth section and select the GET/auth/visitor endpoint. 
- Click on the Try It Out button
- Now, click on the Execute button. Wait for the response to be shown (hope so!).
- If it worked as expected, you now should see something like this in the response body:

{
  "token": "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzUzNDkyMTcsImV4cCI6MTU3NTM1MDQxN30.aW4e4nspoqyhETnTIxpHWMVuGzex9TyZRMlHxJM2IhU"
}

- Copy the value of the token attribute that you just generated, it means: eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzUzNDkyMTcsImV4cCI6MTU3NTM1MDQxN30.aW4e4nspoqyhETnTIxpHWMVuGzex9TyZRMlHxJM2IhU
- That's it! Now you can use that token to sent it in the earthquake endpoints described in the next section.


USING THE APPLICATION: Using earthquake endpoints

- Enter to http://localhost:8097/usgs-interface/swagger-ui.html and there you'll see a list of endpoints exposed by the application under the section Earthquakes
- Select whatever the endpoint you want to check and click on the Try it out button.
- Then, according the each endpoint you are going to be required to put some parameters before send the request.
- Each earthquake endpoint has an Authorization parameter: Don't forget to paste there the JWT you just copied before when you executed the auth/visitor endpoint.
- And that's all! If you have a doubt, write to alexis.ve@gmail.com

 
ABOUT THE COMMONS LIBRARY (https://github.com/thisisalexis/common)

This project is using an external commons library which is also part of the source code of the application but is maintained in a separate repository to facilitate its reuse.

https://github.com/thisisalexis/common

If you want to see commons library source code, follow the next steps:

- Open your git shell
- Type git clone https://github.com/thisisalexis/common.git
- That's all! You can check the code of the library.

WHAT IS MISSING? 

Yep, some requirements are missing!. It was a veeeeeery long project, and since I love to sleep and having a life, not all of the requirements could be covered. A list of those are specified bellow.

- Unit Tests: I know how to implement them using JUnit and Mockito, but... Did you see how many classes and methods does this project (and the common library subproject) has? Too many, and I would have needed at least and entire day to develop all of the required unit tests.
- You guys requrest to develop and endpoint to query earthquakes by country, but the USGS service does not allow to search by country. But it allows to search by longitude and latitude coordinates, and since every country has those coordinates, well, I develop and endpoint that allows you to search events by longitude and latitude.

That's it!

Alexis Bravo