package cl.thisisalexis.globallogicchallenge.api.controller.auth;

import cl.thisisalexis.common.core.workflow.ExecutorResponse;
import cl.thisisalexis.globallogicchallenge.api.controller.AbstractApiControllerTest;
import cl.thisisalexis.globallogicchallenge.api.model.AuthorizationResponse;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

/**
 * This class is use to contain all unit tests for AuthApiController class
 *
 * @author Alexis Bravo
 */
public class AuthApiControllerTest extends AbstractApiControllerTest<AuthApiController, AuthDocumentedApi> {

    @InjectMocks
    public AuthApiController testedApi;

    @Override
    public AuthApiController getTestedApi() {
        return testedApi;
    }

    @Override
    public Class<AuthDocumentedApi> getDocumentedApiType() {
        return AuthDocumentedApi.class;
    }

    @Override
    @BeforeEach
    public void prepareBeforeEachTest() {
        initMocks();
        Mockito.doNothing().when(apiWorkflowExecutor).setExecutor(Mockito.any());
        Mockito.when(apiWorkflowExecutor.execute()).thenReturn(ResponseEntity.ok(new AuthorizationResponse("This is a random text that does not affect anything")));
    }

    @Test
    public void getVisitorAuthorizationMethod_callMethodAndGetValidResponse() {
        ResponseEntity<ExecutorResponse> apiResponse = getTestedApi().getVisitorAuthorization();
        Assert.notNull(apiResponse);
        Assert.isInstanceOf(ResponseEntity.class, apiResponse);
        Assert.notNull(apiResponse.getBody());
        Assert.isInstanceOf(ExecutorResponse.class, apiResponse.getBody(), "Api Response Body is not a valid type: any ExecutorResponse");
    }

}