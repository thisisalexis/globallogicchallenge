package cl.thisisalexis.globallogicchallenge.api.controller;

import cl.thisisalexis.common.core.api.AbstractApiController;
import cl.thisisalexis.common.core.workflow.ApiWorkflowExecutor;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.bind.annotation.RestController;

/**
 * This abstract class is meant to be a template for all classes that acts as ApiRest classes containers and to implement
 * common unit tests for all of this kind of classes
 *
 * @author Alexis Bravo
 */
public abstract class AbstractApiControllerTest<ApiType, DocumentedApiType> {

    @Mock
    public ApiWorkflowExecutor apiWorkflowExecutor;

    public abstract void prepareBeforeEachTest();

    protected void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    protected abstract ApiType getTestedApi();

    protected abstract Class<DocumentedApiType> getDocumentedApiType();

    @Test
    public final void apiControllerClass_isApiWellDocumentedAndFollowsApiDocumentationMetrics() {
        Assert.isInstanceOf(getDocumentedApiType(), getTestedApi(), "This class is not adding API documentation as required");
        Assert.isTrue(numberOfDeclaredMethodsInApiControllerClass() == numberOfDeclaredAndDocumentedApiMethodsInDocumentedType(), "There are methods that are not being documented in this API");
    }

    private final int numberOfDeclaredMethodsInApiControllerClass() {
        return getTestedApi().getClass().getDeclaredMethods().length;
    }

    private final int numberOfDeclaredAndDocumentedApiMethodsInDocumentedType() {
        return getDocumentedApiType().getMethods().length;
    }

    @Test
    public final void apiControllerClass_isTestedClassAValidRestApiWhichImplementsExpectedClasses() {
        Assert.isInstanceOf(AbstractApiController.class, getTestedApi(), getTestedApi().getClass().getName() + " must be a AbstractApiController Type");
        Assert.isTrue(getTestedApi().getClass().getAnnotationsByType(RestController.class).length == 1);
    }

}
