package cl.thisisalexis.globallogicchallenge.api.controller.earthquake;

import cl.thisisalexis.globallogicchallenge.api.controller.AbstractApiControllerTest;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;

/**
 * This class is use to contain all unit tests for EarthquakeApiController class
 *
 * @author Alexis Bravo
 */
public class EarthquakeApiControllerTest extends AbstractApiControllerTest<EarthquakeApiController, EarthquakeDocumentedApi> {

    @InjectMocks
    EarthquakeApiController testedApi;

    @Override
    @BeforeEach
    public void prepareBeforeEachTest() {
        initMocks();
    }

    @Override
    public EarthquakeApiController getTestedApi() {
        return testedApi;
    }

    @Override
    protected Class<EarthquakeDocumentedApi> getDocumentedApiType() {
        return EarthquakeDocumentedApi.class;
    }

}
