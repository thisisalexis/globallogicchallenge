package cl.thisisalexis.globallogicchallenge.service.earthquake;

import cl.thisisalexis.common.core.exception.AbstractAppException;
import cl.thisisalexis.globallogicchallenge.api.model.FeaturesResponse;
import cl.thisisalexis.globallogicchallenge.model.earthquake.EarthquakeQueryFilter;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Features;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Geometry;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Properties;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.model.Query;
import cl.thisisalexis.globallogicchallenge.rest.usgs.earthquake.query.service.USGSEventQueryWebService;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is meant to implement unit tests for EarthquakeService class
 *
 * @author Alexis Bravo
 */
public class EarthquakeServiceTest {

    private static final int DEFAULT_NUMBER_OF_FEATURES_FOR_OK_RESPONSE = 2;

    @Mock
    public USGSEventQueryWebService usgsEventQueryWebService;

    @InjectMocks
    public EarthquakesService testedService;

    @BeforeEach
    public void prepareBeforeEachTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void executeService_USGSEventQueryWebServiceResponseIsOkAndNotNull() throws AbstractAppException {
        Query uSGSEventQueryWebServiceMockedResponse = mockUSGSEarthquakeQueryRestServiceOkResponse();
        Mockito.when(usgsEventQueryWebService.execute(Mockito.any())).thenReturn(uSGSEventQueryWebServiceMockedResponse);
        EarthquakeQueryFilter testedServiceParamFilter = new EarthquakeQueryFilter();
        FeaturesResponse response = testedService.execute(testedServiceParamFilter);
        Assert.notNull(response);
        Assert.isTrue(response.getFeatures().size() == DEFAULT_NUMBER_OF_FEATURES_FOR_OK_RESPONSE);
    }

    @Test
    public void executeService_USGSEventQueryWebServiceResponseIsNull() throws AbstractAppException {
        Query uSGSEventQueryWebServiceMockedResponse = null;
        Mockito.when(usgsEventQueryWebService.execute(Mockito.any())).thenReturn(uSGSEventQueryWebServiceMockedResponse);
        FeaturesResponse testedServiceResponse = testedService.execute(new EarthquakeQueryFilter());
        Assert.notNull(testedServiceResponse, "Service returns a null value when the USGS Query service returns a null response");
        Assertions.assertEquals(testedServiceResponse.getFeatures().size(), 0, "Number of feature or events returned by service is not zero when the USGS query service returns a null response.");
    }

    @Test
    public void executeService_USGSThrowsRestClientExceptionAndExecuteServicesDoesNotCatchIt() {
        Mockito.when(usgsEventQueryWebService.execute(Mockito.any())).thenThrow(RestClientException.class);
        Assertions.assertThrows(RestClientException.class, () -> testedService.execute(new EarthquakeQueryFilter()));
    }

    private Query mockUSGSEarthquakeQueryRestServiceOkResponse() {
        Query mockedQuery = new Query();

        List<Features> features = new ArrayList<>();
        Features feature = new Features();
        feature.setId(String.valueOf(1));
        feature.setType("E");
        feature.setGeometry(new Geometry());
        feature.setProperties(new Properties());

        for (int i = 0; i < DEFAULT_NUMBER_OF_FEATURES_FOR_OK_RESPONSE; i++) {
            features.add(feature);
        }
        mockedQuery.setFeatures(features);

        return mockedQuery;
    }

}
